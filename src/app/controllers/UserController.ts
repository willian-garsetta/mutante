import { Request, Response } from 'express'
import  Api from '../services/api'
import User from '../models/User'
import Sort from '../services/Sort'

class UserController {
  public async index (req: Request, res: Response): Promise<Response> {
 const { data: users} = await Api.get('/users')
 const websites = users.map((user: User): string => user.website)

    return res.json(users)
  }

  public async searchWebsites (req: Request, res: Response): Promise<Response> {
    const { data: users} = await Api.get('/users')
    const websites = users.map((user: User): string => user.website)

    return res.json(websites)
  }

  public async userInfo (req: Request, res: Response): Promise<Response> {
    const { data: users} = await Api.get('/users')
    const userInfo = users.map((user: User) : User => ({
      name: user.name,
      email: user.email,
      company: user.company
    }))

    const sortedUsers = Sort.sort(userInfo)

    return res.json(sortedUsers)
  }

  public async searchAddress (req: Request, res: Response): Promise<Response> {
    const { data: users} = await Api.get('/users')
    const filterUsers = users.filter(({ address: { suite } }: User): boolean =>
      suite.toLowerCase().includes('suite')
    )

    return res.json(filterUsers)
  }
}

export default new UserController()
