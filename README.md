# Mutant Challenge

The challange consist on build an application to fetch data from [Json Place Holder - Users](https://jsonplaceholder.typicode.com/users)

## How To Run the Project

### Locally

#### Prerequisite

What you need and how to intall them:

- [Node.js](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/pt-BR/)
- [GIT](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

#### Clone Repository

On the directory of your choice run:

```bash
  git clone https://willian-garsetta@bitbucket.org/willian-garsetta/mutante.git
```

#### Install Dependencies

```bash
  cd mutant-test
  yarn install
```

#### Run
On your terminal, use the command `yarn Dev` to create server access.

You can access `localhost:8080` with the routes bellow on your browser or some client applicantion that you prefer like `Insominia` or `Postman`

## Routes

| Endpoint                              | Description                            |
| ------------------------------------- | -------------------------------------- |
| GET `/api/users` **PUBLIC**           | Get All users                          |
| GET `/api/websites` **PUBLIC**        | Get All Websites                       |
| GET `/api/usersInfo `**PUBLIC**       | Get basicinfos asc sort                |
| GET `/api/address` **PUBLIC**         | Get all users with `suite` on address. |

## Built With

- [Node.js](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/pt-BR/)
- [Express](https://expressjs.com/pt-br/)
- [ESLint](https://eslint.org/)
- [Mogan](https://github.com/expressjs/morgan)
- [Cors](https://github.com/expressjs/cors)
- [Axios](https://github.com/axios/axios)
- [Sucrase](https://github.com/alangpierce/sucrase)
- [Typescript](https://www.typescriptlang.org/)
- [Prettier](https://prettier.io/)
- [Nodemon](https://nodemon.io/)
