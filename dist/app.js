"use strict"; function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }Object.defineProperty(exports, "__esModule", {value: true});var _express = require('express'); var _express2 = _interopRequireDefault(_express);
var _morgan = require('morgan'); var _morgan2 = _interopRequireDefault(_morgan);

var _routes = require('./routes'); var _routes2 = _interopRequireDefault(_routes);

class App {
  

   constructor () {
    this.express = _express2.default.call(void 0, )

    this.middlewares()
    this.routes()
  }

   middlewares () {
    this.express.use(_express2.default.json())
    this.express.use(_morgan2.default.call(void 0, 'dev'))
  }

   routes () {
    this.express.use(_routes2.default)
  }
}

exports. default = new App().express
