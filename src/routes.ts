import { Router } from 'express'
import UserController from './app/controllers/UserController'

const routes = Router()

routes.get('/users', UserController.index)
routes.get('/websites', UserController.searchWebsites)
routes.get('/usersInfo', UserController.userInfo)
routes.get('/address', UserController.searchAddress)

export default routes
